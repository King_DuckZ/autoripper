=begin
Copyright 2011 Michele Santullo

This file is part of 'audioripper'.

audioripper is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

audioripper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with audioripper; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'set'
require 'Helpers.rb'

module RipHelpers
  def self.IsAlfaNum(strChar)
    return false if strChar.nil? || strChar.length == 0
    strFirstChar = strChar[0]

    return strFirstChar >= "a"[0] && strFirstChar <= "z"[0] ||
      strFirstChar >= "A"[0] && strFirstChar <= "Z"[0] ||
      strFirstChar >= "0"[0] && strFirstChar <= "9"[0]
  end

  def self.DropAndTrim(itData, objFind, objTrimLeft, objTrimRight)
    bRecomposeString = false
    itRet = nil

    if Helpers.CompareRubyVersion(1, 9, 0) <= 0 then #at least v1.9.0?
      itRet = itData.dup
    else
      if itData.is_a? String then
        bRecomposeString = true
        itRet = itData.scan(/./mu)
      else
        itRet = itData.dup
      end
    end

    nIndex = 0
    nPrevIndex = 0
    nCurrIndex = nil
    while nIndex < itRet.length && (nCurrIndex = itRet[nIndex..-1].index(objFind)) do
      nIndex += nCurrIndex
      nDropLeft = nIndex
      while nDropLeft - 1 > nPrevIndex && itRet[nDropLeft - 1] == objTrimLeft do
        nDropLeft -= 1
      end

      nDropRight = nIndex
      while nDropRight + 1 < itRet.length && itRet[nDropRight + 1] == objTrimRight do
        nDropRight += 1
      end

      itRet.slice!(nDropLeft, nDropRight - nDropLeft + 1)
      nIndex -= nIndex - nDropLeft
      nPrevIndex = nIndex
    end

    itRet = itRet.join("") if bRecomposeString
    itRet
  end

  def self.GetCleanNameForFilename(strName, bDropParentheses, strReplaceSpaces)
    regParentheses = /\(.+?\)/u
    strRet = strName
    if bDropParentheses then
      strRet.gsub! regParentheses, ""
      strRet.strip!
    end

    aTranslations = [
      ["àáăāä", "a"], ["ÀÁĂĀÄ", "A"],
      ["èéĕēë", "e"], ["ÈÉĔĒË", "E"],
      ["ìíĭīï", "i"], ["ÌÍĬĪÏ", "I"],
      ["òóŏōö", "o"], ["ÒÓŎŌÖ", "O"],
      ["ùúŭūü", "u"], ["ÙÚŬŪÜ", "U"],
      ["ÿȳ", "y"], ["ŸȲ", "Y"],
      ["ñ", "n"], ["Ñ", "N"],
      ["ç", "c"], ["Ç", "C"],
      ["&", "and"],
#       ["\"'()[]{}<>|\\/!?§°*£$€%=^@#;,:¹²³¼½¬`~", nil],
      [".\"'()[]{}<>|\\/!?§°*£$€%=^@#;,:¹²³¼½¬`~", ""],
      ["\t\n", " "]
    ]

    aTranslations.each do |strForbidden, strReplace|
      strForbidden.scan(/./mu).each do |strChar|
        unless strReplace.nil? then
          strRet.gsub! strChar, strReplace
        else
          strRet = DropAndTrim(strRet, strChar, " ", " ")
        end
      end
    end
    strRet.gsub! /\s{2,}/u, " "
    strRet.gsub! " ", strReplaceSpaces unless strReplaceSpaces.nil?
    strRet.strip
  end

  def self.GetMostMiddleOccurrence(strString, strChar)
    nIndex = 0
    aIndices = Array.new
    while (nIndex = strString.index(strChar, nIndex)) do
      aIndices << nIndex
      nIndex += 1
    end
    if aIndices.empty? then
      return nil
    elsif aIndices.length == 1 then
      return aIndices.first
    else
      nMid = strString.length / 2
      return aIndices.sort {|nA, nB| (nA - nMid).abs <=> (nB - nMid).abs }.first
    end
  end

  def self.GuessSplitCharacter(aNames)
    #Splitting character is something that should be found in any string in
    #aNames. It's not a space or some other common character.

    hEncounteredCharas = Hash.new(0)
    aNames.each do |strName|
      sEncounteredInName = Set.new
      strName.each_char {|strChar| sEncounteredInName.add strChar }
      sEncounteredInName.each do |strChar|
        hEncounteredCharas[strChar] += 1
      end
    end

    #With that said, only characters with a count of aNames.length are
    #good candidates for being the separator.
    hEncounteredCharas.delete_if {|strChar, nCount| nCount != aNames.length || IsAlfaNum(strChar) }
    if hEncounteredCharas.empty? then
      return nil
    elsif hEncounteredCharas.length == 1 then
      return hEncounteredCharas.keys.first
    end

    sCandidates = Set.new hEncounteredCharas.keys
    sItsNot = Set.new [' ', '\n', '\t', '&', '?']
    unless sCandidates.subset? sItsNot then
      sCandidates = sCandidates - sItsNot
      return sCandidates.to_a.first if sCandidates.length == 1
    end
    hEncounteredCharas = nil

    #To select the best candidate we try to find the symbol that seems to be
    #in the middle more often
    aMids = aNames.collect {|strName| strName.length.to_f / 2.0 }
    fLowestScore = -10.0
    strRet = nil
    sCandidates.each do |strCandidate|
      fScore = 0.0
      aNames.length.times do |z|
        nPos = GetMostMiddleOccurrence(aNames[z], strCandidate) + 1
#         puts "#{strCandidate} in #{aNames[z]}: #{nPos} (len: #{aNames[z].length}, mid: #{aNames[z].length / 2})"
        fScore += (nPos.to_f - aMids[z]).abs * aNames[z].count(strCandidate).to_f
      end
      if fScore < fLowestScore || fLowestScore < 0.0
        fLowestScore = fScore
        strRet = strCandidate
      end
    end

    #At this point, just return the element with the _lowest_ score.
    return strRet
  end

  def self.SplitNames(aNames, procCallbackOnFailure, strSplit=nil)
    nSize = aNames.length
    aArtists = Array.new(nSize)
    aTitles = Array.new(nSize)
    strSplitChar = strSplit || GuessSplitCharacter(aNames)
    return nil, nil if strSplitChar.nil?

    nSize.times do |z|
      nSplitAt = GetMostMiddleOccurrence(aNames[z], strSplitChar)
      if nSplitAt.nil? then
        aArtists[z], aTitles[z] = procCallbackOnFailure.call(aNames[z], strSplitChar)
      else
        aArtists[z] = aNames[z][0..(nSplitAt-1)].rstrip
        aTitles[z] = aNames[z][(nSplitAt+1)..-1].lstrip
      end
    end
    return aArtists, aTitles
  end

  class CFilenameMaker
    def initialize(strExtension, bDropParentheses, strZapSpaces)
      @strExtension = nil
      if strExtension then
        raise Exception.new("Extension must be a string, received a #{strExtension.class.name}") unless strExtension.is_a?(String)
        if strExtension.length > 0 then
          if strExtension[0] == "."[0] then
            @strExtension = strExtension
          else
            @strExtension = "." + strExtension
          end
        end
      end
      @bDropParentheses = (bDropParentheses ? true : false)
      @strZapSpaces = strZapSpaces
      self
    end

    def GetFilename(strRawName)
      RipHelpers.GetCleanNameForFilename(strRawName, @bDropParentheses, @strZapSpaces) + @strExtension
    end
  end
end
