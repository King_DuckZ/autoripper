=begin
Copyright 2011 Michele Santullo

This file is part of 'audioripper'.

audioripper is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

audioripper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with audioripper; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'forwardable'

#Dynamically load encoders
module EncoderFactory
  class CEncoderInfo
    extend Forwardable
    SEncoderInfo = Struct.new(:name, :extension, :classobj)

    def_delegator :@aRequiredModules, :each, :each_module
    def_delegator :@aRequiredModules, :[], :[]

    def initialize(aModuleNames)
      @aRequiredModules = Array.new
      aModuleNames.each do |strModule|
        newObj = SEncoderInfo.new
        objModule = Kernel.const_get(strModule)
        newObj.extension = objModule.GetExtension()
        newObj.classobj = objModule::CEncoder
        newObj.name = strModule
        @aRequiredModules << newObj
      end
      self
    end

    def GetByName(strName)
      @aRequiredModules.each do |encoderInfo|
        return encoderInfo if encoderInfo.name == strName
      end
      return nil
    end
  end
end

aReqMods = Array.new
Dir.glob(File.join($APP_PATH, "*Encoder.rb")).each do |strRequire|
  begin
    require strRequire
  rescue
    raise if $DEBUG
    next
  else
    aReqMods << File.basename(strRequire, ".rb")
  end
end
$objEncodersInfo = EncoderFactory::CEncoderInfo.new(aReqMods)
aReqMods = nil

module EncoderFactory
  class CFactory
    def initialize()
      self
    end

    def GetValidValues()
      aValidValues = Array.new
      $objEncodersInfo.each_module do |objModInfo|
        aValidValues << objModInfo.name
      end
      return aValidValues
    end

    def CreateEncoderFromName(strName, fQuality, objFilenameMaker)
      objRetVal = $objEncodersInfo.GetByName(strName)
      raise Exception.new("Can't find a module by the name \"#{strName}\"") if objRetVal.nil?
      return objRetVal.classobj.new(fQuality, objFilenameMaker)
    end

    def GetExtensionFromName(strName)
      objRetVal = $objEncodersInfo.GetByName(strName)
      raise Exception.new("Can't find a module by the name \"#{strName}\"") if objRetVal.nil?
      return objRetVal.extension
    end
  end
end
