=begin
Copyright 2011 Michele Santullo

This file is part of 'audioripper'.

audioripper is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

audioripper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with audioripper; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'RipHelpers.rb'
require 'forwardable'
require 'iconv'

module RipTools
  SAlbumInfo = Struct.new(:artist, :name, :duration, :genre, :year, :trackcount, :extended)
  STrackInfo = Struct.new(:artist, :title, :extended, :tracknum)

  SelectFirst = Proc.new do |freedb|
    freedb.get_result(0)
  end

  class CDisc
    extend Forwardable

    def_delegator :@tracks, :each, :each_track
    attr_reader :album, :tracks

    def is_ready? ()
      return !@album.nil?
    end

    def initialize(strDevice, objDisambiguator, bSplitArtistTitle, strSplitCharacter)
      freedb = Freedb.new(strDevice)
      nRetry = 10
      begin
        freedb.fetch
      rescue
        nRetry -= 1
        if nRetry > 0 then
          sleep(0.2)
          retry
        else
          raise Exception.new("Can't fetch from freedb")
        end
      end
      result = objDisambiguator.call(freedb)

      @album = SAlbumInfo.new
      @album.artist = ConvFreedbToUtf8(freedb.artist)
      @album.extended = ConvFreedbToUtf8(freedb.ext_infos)
      @album.genre = ConvFreedbToUtf8(freedb.genre)
      @album.trackcount = freedb.tracks.length
      @album.name = ConvFreedbToUtf8(freedb.title)
      @album.year = freedb.year

      rawTitles = Array.new(@album.trackcount) {|z| ConvFreedbToUtf8(freedb.tracks[z]["title"]) }
      aArtists, aTitles = nil, nil
      if bSplitArtistTitle then
        procOnSplitFailure = Proc.new do |strText, strSplitCharacter|
          $stderr.puts "Couldn't split \"#{strText}\": no \"#{strSplitCharacter}\" found"
          next @album.artist, strText
        end
        aArtists, aTitles = RipHelpers.SplitNames(rawTitles, procOnSplitFailure, strSplitCharacter)
        if aArtists.nil? && aTitles.nil? then
          $stderr.puts "Can't guess a split character, ignoring"
        end
      end
      if aArtists.nil? || aTitles.nil? then
        aArtists = Array.new(@album.trackcount) { @album.artist }
        aTitles = rawTitles
      end
#       aArtists.length.times {|z| puts "#{z+1}\t#{rawTitles[z]} -->\t\"#{aArtists[z]}\"\t\"#{aTitles[z]}\""}
      rawTitles = nil

      @tracks = Array.new(@album.trackcount) do |z|
        hCurrTrack = freedb.tracks[z]
        STrackInfo.new(aArtists[z], aTitles[z], ConvFreedbToUtf8(hCurrTrack["ext"]), z + 1)
      end
      aArtists = nil
      aTitles = nil
    ensure
      freedb.close()
    end

    def ConvFreedbToUtf8(strText)
      Iconv.iconv("utf-8", "ISO-8859-1", strText).first
    end

    private :ConvFreedbToUtf8
  end

  class CEncoderBase
    attr_accessor :album, :artist, :compilation, :discnumber, :quality, :genre
    def initialize(fQuality, objFilenameMaker)
      @album, @artist = nil, nil, nil
      @compilation = false
      @discnumber = nil
      @quality = (fQuality.is_a?(Float) ? fQuality : fQuality.to_f)
      raise Exception.new("Quality must be a positive number.") if fQuality <= 0.0
      @objFilenameMaker = objFilenameMaker
    end

    def discnumber=(nNum); @discnumber = (nNum.nil? || nNum.is_a?(String) ? nNum : nNum.to_s); end
    def compilation=(bVal); @compilation = (bVal ? true : false); end

    def GetOutputFile(objTrackInfo, nTrackNum)
      strTracknum = nTrackNum.to_s().rjust(2, "0")
      strDirtyName = "#{strTracknum} - #{objTrackInfo.title}"
      @objFilenameMaker.GetFilename(strDirtyName)
    end
  end

  class CRipper
    attr_reader :device
    def initialize(strDevice)
      @device = strDevice || "/dev/cdrom"
      @pid = nil
    end

    def Rip(objEncoder, objTrackInfo, nTrackNum, nTrackNumForMetadata)
      if objEncoder.IsStdinSupported()
        @pid = fork do
          system("#{GetCommand(nTrackNum)} | #{objEncoder.GetEncodeCommand(nil, objTrackInfo, nTrackNumForMetadata)}")
        end
        Process.wait(@pid)
      else
        raise Exception.new("Not implemented yet")
      end
      @pid = nil
      nil
    end

    def GetCommand(nTrackNum)
      "cdparanoia --output-wav --quiet --force-cdrom-device \"#{@device}\" '#{nTrackNum}' -"
    end

    def Stop()
      if @pid then
        Process.kill("INT", @pid)
      end
    end

    private :GetCommand
  end
end
