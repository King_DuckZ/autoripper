=begin
Copyright 2011 Michele Santullo

This file is part of 'audioripper'.

audioripper is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

audioripper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with audioripper; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'RipTools.rb'

module OggEncoder
  def self.GetExtension()
    "ogg"
  end

  class CEncoder < RipTools::CEncoderBase
    def initialize(fQuality, objFilenameMaker)
      super(fQuality, objFilenameMaker)
    end

    def GetEncodeCommand(strFilename, objTrackInfo, nTrackNum)
      nLocalTrackNum = nTrackNum || objTrackInfo.tracknum
      strRet = "oggenc --quiet --utf8 " +
        "--quality=#{self.quality} " +
        "-o \"#{self.GetOutputFile(objTrackInfo, nLocalTrackNum)}\" " +
        "--artist \"#{objTrackInfo.artist}\" " +
        "--genre \"#{@genre}\" " +
        "--tracknum #{nLocalTrackNum} " +
        "--title \"#{objTrackInfo.title}\" " +
        "--album \"#{@album}\" " +
        "--comment \"COMPILATION=#{@compilation ? 1 : 0}\" " +
        "--date \"#{@year}\" "

        if !@discnumber.nil? && @discnumber.length > 0 && @discnumber != "0" then
          strRet += "--comment \"DISCNUMBER=#{@discnumber}\" "
        end
        return strRet.rstrip + " " + (strFilename ? strFilename : "-")
    end

    def IsStdinSupported()
      true
    end
  end
end
